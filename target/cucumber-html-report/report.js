$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/BookingUp.feature");
formatter.feature({
  "line": 1,
  "name": "Booking Up",
  "description": "",
  "id": "booking-up",
  "keyword": "Feature"
});
formatter.before({
  "duration": 2831709514,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "FireFox is launched",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingUpSD.firefox_is_launched()"
});
formatter.result({
  "duration": 10751645787,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "make a booking up with too short and invalid card number",
  "description": "",
  "id": "booking-up;make-a-booking-up-with-too-short-and-invalid-card-number",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "Ryanair page is loaded",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I enter departure and destination airport",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I enter fly out and fly back date",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I click continue button",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "Booking home page is loaded",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "I select flights and click continue button",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "Booking extras page is loaded",
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "I click check out and thanks button",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "Booking payment page is loaded",
  "keyword": "Then "
});
formatter.step({
  "line": 16,
  "name": "I login with valid credentials",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "I enter passenger and contact details",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "I enter payment method with too short card number",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "I enter billing address",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "I click pay now button",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "I get error message that card number is too short",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "I enter payment method with invalid card number",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "I click pay now button",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "I get error message that card number is invalid",
  "keyword": "Then "
});
formatter.match({
  "location": "BookingUpSD.ryanair_page_is_loaded()"
});
formatter.result({
  "duration": 645020097,
  "status": "passed"
});
formatter.match({
  "location": "BookingUpSD.i_enter_departure_and_destination_airport()"
});
formatter.result({
  "duration": 5026112650,
  "status": "passed"
});
formatter.match({
  "location": "BookingUpSD.i_enter_fly_out_and_fly_back_date()"
});
formatter.result({
  "duration": 6278021623,
  "status": "passed"
});
formatter.match({
  "location": "BookingUpSD.i_click_continue_button()"
});
formatter.result({
  "duration": 6197170941,
  "status": "passed"
});
formatter.match({
  "location": "BookingUpSD.booking_home_page_is_loaded()"
});
formatter.result({
  "duration": 66770395,
  "status": "passed"
});
formatter.match({
  "location": "BookingUpSD.i_select_flights()"
});
formatter.result({
  "duration": 13287368746,
  "status": "passed"
});
formatter.match({
  "location": "BookingUpSD.booking_extras_page_is_loaded()"
});
formatter.result({
  "duration": 748279245,
  "status": "passed"
});
formatter.match({
  "location": "BookingUpSD.i_click_check_out_button()"
});
formatter.result({
  "duration": 2412294932,
  "status": "passed"
});
formatter.match({
  "location": "BookingUpSD.booking_payment_page_is_loaded()"
});
formatter.result({
  "duration": 264867677,
  "status": "passed"
});
formatter.match({
  "location": "BookingUpSD.i_login_with_valid_credentials()"
});
formatter.result({
  "duration": 4095234798,
  "status": "passed"
});
formatter.match({
  "location": "BookingUpSD.i_enter_passenger_and_contact_details()"
});
formatter.result({
  "duration": 3245622421,
  "status": "passed"
});
formatter.match({
  "location": "BookingUpSD.i_enter_payment_method_with_too_short_card_number()"
});
formatter.result({
  "duration": 1641164625,
  "status": "passed"
});
formatter.match({
  "location": "BookingUpSD.i_enter_billing_address()"
});
formatter.result({
  "duration": 1553918762,
  "status": "passed"
});
formatter.match({
  "location": "BookingUpSD.i_click_pay_now_button()"
});
formatter.result({
  "duration": 1155313074,
  "status": "passed"
});
formatter.match({
  "location": "BookingUpSD.i_get_error_message_that_card_number_is_too_short()"
});
formatter.result({
  "duration": 5048449422,
  "status": "passed"
});
formatter.match({
  "location": "BookingUpSD.i_enter_payment_method_with_invalid_card_number()"
});
formatter.result({
  "duration": 2212256665,
  "status": "passed"
});
formatter.match({
  "location": "BookingUpSD.i_click_pay_now_button()"
});
formatter.result({
  "duration": 1147409387,
  "status": "passed"
});
formatter.match({
  "location": "BookingUpSD.i_get_error_message_that_card_number_is_invalid()"
});
formatter.result({
  "duration": 42571111,
  "status": "passed"
});
});