 Feature: Booking Up

  Background: 
    Given FireFox is launched

  Scenario: make a booking up with too short and invalid card number
    Given Ryanair page is loaded
    When I enter departure and destination airport
    And I enter fly out and fly back date
    And I click continue button
    Then Booking home page is loaded
    When I select flights and click continue button
    Then Booking extras page is loaded
    When I click check out and thanks button
    Then Booking payment page is loaded
    When I login with valid credentials
    And I enter passenger and contact details
    And I enter payment method with too short card number
    And I enter billing address
    And I click pay now button
    Then I get error message that card number is too short
    When I enter payment method with invalid card number
    And I click pay now button
    Then I get error message that card number is invalid
