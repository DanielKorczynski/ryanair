package com.dko.ryanair.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class BookingHomePage extends PageObject {

	// locators
	@FindBy(className = "plane") 
	List<WebElement> planeButton;

	@FindBy(css = "button[translate='trips.summary.buttons.btn_select']")
	List<WebElement> fareSelectButton;

	@FindBy(css = "button[translate='trips.summary.buttons.btn_continue']")
	private WebElement continueButton;

	// constructor
	public BookingHomePage(WebDriver driver) {
		super(driver);
		System.out.print("waiting for the page...");
		wait.until(ExpectedConditions.elementToBeClickable(planeButton.get(0)));
		System.out.println(" done");
	}

	public boolean isInitialized() {
		return planeButton.get(0).isDisplayed();
		// return heading.getText().toString().contains("Title");
	}

	public void selectFlight(int flightId) {
		planeButton.get(flightId).click();
		wait.until(ExpectedConditions.elementToBeClickable(fareSelectButton.get(0)));
	}

	public void clickFareSelectButton(int fareSelect) {
		fareSelectButton.get(fareSelect).click();
	}

	public BookingExtrasPage clickContinueButton() {
		continueButton.click();
		return new BookingExtrasPage(driver);
	}

}
