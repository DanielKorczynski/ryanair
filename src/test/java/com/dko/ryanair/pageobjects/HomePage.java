package com.dko.ryanair.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class HomePage extends PageObject {

	// locators
	@FindBy(css = "input[placeholder='Departure airport']")
	private WebElement departureAirportField;

	@FindBy(css = "input[placeholder='Destination airport']")
	private WebElement destinationAirportField;

	@FindBy(css = "span[translate='common.buttons.continue']")
	private WebElement continueButton;

	@FindBy(className = "dd") 
	List<WebElement> dayField;

	@FindBy(className = "mm") 
	List<WebElement> monthField;

	@FindBy(className = "yyyy") 
	List<WebElement> yearField;

	@FindBy(css = "span[translate='common.buttons.lets_go']")
	private WebElement letsGoButton;

	// constructor
	public HomePage(WebDriver driver) {
		super(driver);
		System.out.print("waiting for the page...");
		wait.until(ExpectedConditions.elementToBeClickable(departureAirportField));
		System.out.println(" done");
	}

	public boolean isInitialized() {
		return departureAirportField.isDisplayed();
//		return heading.getText().toString().contains("Title");
	}

	public void enterAirport(String departure, String destination) {
		departureAirportField.click();
		departureAirportField.clear();
		departureAirportField.sendKeys(departure);
		destinationAirportField.click();
		destinationAirportField.clear();
		destinationAirportField.sendKeys(destination);
		clickContinueButton();
	}

	public void clickContinueButton() {
		continueButton.click();
	}

	public void enterFlyOutDate(String dd, String mm, String yyyy) throws InterruptedException {
		dayField.get(0).click();
		dayField.get(0).clear();
		dayField.get(0).sendKeys(dd);
		monthField.get(0).clear();
		monthField.get(0).sendKeys(mm);
		yearField.get(0).clear();
		yearField.get(0).sendKeys(yyyy);
	}

	public void enterFlyBackDate(String dd, String mm, String yyyy) throws InterruptedException {
		dayField.get(1).click();
		dayField.get(1).clear();
		dayField.get(1).sendKeys(dd);
		monthField.get(1).clear();
		monthField.get(1).sendKeys(mm);
		yearField.get(1).clear();
		yearField.get(1).sendKeys(yyyy);
	}

	public BookingHomePage clickLetsGoButton(){
		letsGoButton.click();
		return new BookingHomePage(driver);
	}

}
