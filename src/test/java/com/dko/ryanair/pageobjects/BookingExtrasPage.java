package com.dko.ryanair.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class BookingExtrasPage extends PageObject {

	// locators
	@FindBy(css = "button[translate='trips.summary.buttons.btn_checkout']")
	private WebElement checkOutButton;

	@FindBy(css = "button[translate='trips.summary.seat.prompt.popup.reject']")
	private WebElement okThanksButton;

	// constructor
	public BookingExtrasPage(WebDriver driver) {
		super(driver);
		System.out.print("waiting for the page...");
		wait.until(ExpectedConditions.elementToBeClickable(checkOutButton));
		System.out.println(" done");
	}

	public boolean isInitialized() {
		return checkOutButton.isDisplayed();
		// return heading.getText().toString().contains("Title");
	}

	public void clickCheckOutButton() {
		checkOutButton.click();
	}

	public BookingPaymentPage clickOkThanksButton() {
		okThanksButton.click();
		return new BookingPaymentPage(driver);
	}

}
