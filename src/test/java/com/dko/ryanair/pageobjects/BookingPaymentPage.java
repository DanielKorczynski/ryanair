package com.dko.ryanair.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class BookingPaymentPage extends PageObject {

	// locators
	@FindBy(css = "span[translate='trips.checkout.passengers.login']")
	private WebElement logInButton;

	@FindBy(css = "input[placeholder='Email address']")
	private WebElement emailField;

	@FindBy(css = "input[placeholder='Password']")
	private WebElement passwordField;

	@FindBy(css = "input[name='remember']")
	private WebElement keepMeLoggedInCheckBox;

	@FindBy(css = "span[translate='MYRYANAIR.AUTHORIZATION.LOGIN.LOGIN_AUTHORIZATION']")
	private WebElement logInAuthorizationButton;

	@FindBy(css = "option[label='Mr']")
	private WebElement titleMr;

	@FindBy(css = "input[placeholder='e.g. John']")
	private WebElement firstNameField;

	@FindBy(css = "input[placeholder='e.g. Smith']")
	private WebElement surnameField;

	@FindBy(css = "option[label='Poland']") 
	List<WebElement> selectCountryPolandButton;

	@FindBy(css = "input[name='phoneNumber']")
	private WebElement phoneNumberField;

	@FindBy(css = "input[placeholder='Enter card number']")
	private WebElement cardNumberField;

	@FindBy(css = "option[label='MasterCard']")
	private WebElement cardTypeMasterCardButton;

	@FindBy(css = "option[label='5']")
	private WebElement expiryMonth5Field;

	@FindBy(css = "option[label='2020']")
	private WebElement expiryYear2020Field;

	@FindBy(css = "input[placeholder='CVV']")
	private WebElement securityCodeField;

	@FindBy(css = "input[placeholder='e.g. John Smith']")
	private WebElement cardholdersNameField;

	@FindBy(css = "input[placeholder='e.g. 21 Sun Lane']")
	private WebElement addressFirstField;

	@FindBy(css = "input[placeholder='e.g. Sun Land']")
	private WebElement addressSecondField;

	@FindBy(css = "input[placeholder='e.g. Dublin']")
	private WebElement cityField;

	@FindBy(css = "input[placeholder='e.g. 12345']")
	private WebElement postcodeField;

	@FindBy(css = "input[name='acceptPolicy']")
	private WebElement acceptPolicyButton;

	@FindBy(css = "button[translate='common.components.payment_forms.pay_now']")
	private WebElement payNowButton;
	
	@FindBy(css = "span[translate='common.components.payment_forms.error_card_number_minlength']")
	private WebElement errorCardNumberTooShortLabel;
	
	@FindBy(css = "span[translate='common.components.payment_forms.error_card_number_luhn']")
	private WebElement errorCardNumberInvalid;
	
	// constructor
	public BookingPaymentPage(WebDriver driver) {
		super(driver);
		wait.until(ExpectedConditions.elementToBeClickable(logInButton));
	}

	public boolean isInitialized() {
		return logInButton.isDisplayed();
		// return heading.getText().toString().contains("Title");
	}

	public void clickLoginButton() {
		logInButton.click();
	}

	public void enterCredentials(String email, String password) {
		emailField.clear();
		emailField.sendKeys(email);
		passwordField.clear();
		passwordField.sendKeys(password);
	}

	public void selectKeepMeLoggedIn(boolean state) {
	    boolean isSelected = keepMeLoggedInCheckBox.isSelected();
		if (state != isSelected) keepMeLoggedInCheckBox.click();
	}
	
	public void clickLogInAuthorizationButton() {
		logInAuthorizationButton.click();
	}
	
	public void enterPassengerDetails(String title, String name, String surname) {
		if (title.equals("Mr")) titleMr.click();
		firstNameField.clear();
		firstNameField.sendKeys(name);
		surnameField.clear();
		surnameField.sendKeys(surname);
	}

	public void enterContactDetails(String phoneNumber) {
		selectCountryPolandButton.get(0).click();
		phoneNumberField.clear();
		phoneNumberField.sendKeys(phoneNumber);
	}

	public void enterPaymentMethod(String cardNumber, String securityCode, String cardholdersName) {
		cardNumberField.clear();
		cardNumberField.sendKeys(cardNumber);
		cardTypeMasterCardButton.click();
		expiryMonth5Field.click();
		expiryYear2020Field.click();
		securityCodeField.clear();
		securityCodeField.sendKeys(securityCode);
		cardholdersNameField.clear();
		cardholdersNameField.sendKeys(cardholdersName);
	}

	public void enterBillingAddress(String addressFirst, String addressSecond, String city, String postcode) {
		addressFirstField.clear();
		addressFirstField.sendKeys(addressFirst);
		addressSecondField.clear();
		addressSecondField.sendKeys(addressSecond);
		cityField.clear();
		cityField.sendKeys(city);
		postcodeField.clear();
		postcodeField.sendKeys(postcode);
		selectCountryPolandButton.get(1).click();
	}

	public void clickPayNowButton(boolean state) {
	    boolean isSelected = acceptPolicyButton.isSelected();
		if (state != isSelected) acceptPolicyButton.click();
		payNowButton.click();
	}
	
	public boolean isErrorTooShortPresent() {
		return errorCardNumberTooShortLabel.isDisplayed();
	}
	
	public boolean isErrorInvalidPresent() {
		return errorCardNumberInvalid.isDisplayed();
	}
	
	public String getErrorTooShortMessage() {
		return errorCardNumberTooShortLabel.getText();
	}
	
	public String getErrorInvalidMessage() {
		return errorCardNumberInvalid.getText(); 	
	}
}
