package com.dko.ryanair.featureRunners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith (Cucumber.class)
@CucumberOptions(
		plugin={"html:target/cucumber-html-report"}, // html report
		features="classpath:features",
		glue={"com.dko.ryanair.stepDefinitions"}
		)
public class BasicTests_JUnit {
}
