package com.dko.ryanair.stepDefinitions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.dko.ryanair.pageobjects.BookingExtrasPage;
import com.dko.ryanair.pageobjects.BookingHomePage;
import com.dko.ryanair.pageobjects.BookingPaymentPage;
import com.dko.ryanair.pageobjects.HomePage;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BookingUpSD {
	
	private static WebDriver driver;
	private HomePage homePage;
	private BookingHomePage bookingHomePage; 
	private BookingExtrasPage bookingExtrasPage;
	private BookingPaymentPage bookingPaymentPage;
	
	@Before
	public static void setUp() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Given("^FireFox is launched$")
	public void firefox_is_launched() throws Throwable {
		driver.get("https://www.ryanair.com/ie/en/");
	}

	@Given("^Ryanair page is loaded$")
	public void ryanair_page_is_loaded() throws Throwable {
		homePage = new HomePage(driver);
		assertTrue(homePage.isInitialized());	
	}

	@When("^I enter departure and destination airport$")
	public void i_enter_departure_and_destination_airport() throws Throwable {
		homePage.enterAirport("Bydgoszcz", "Dusseldorf Weeze");
		Thread.sleep(2000);
		homePage.clickContinueButton();
	}

	@When("^I enter fly out and fly back date$")
	public void i_enter_fly_out_and_fly_back_date() throws Throwable {
		homePage.enterFlyOutDate("05", "06", "2017");
		Thread.sleep(1000);
		homePage.enterFlyBackDate("09", "06", "2017");
		Thread.sleep(2000);
	}

	@When("^I click continue button$")
	public void i_click_continue_button() throws Throwable {
		bookingHomePage = homePage.clickLetsGoButton();
	}

	@Then("^Booking home page is loaded$")
	public void booking_home_page_is_loaded() throws Throwable {
		assertTrue(bookingHomePage.isInitialized());
	}

	@When("^I select flights and click continue button$")
	public void i_select_flights() throws Throwable {
		bookingHomePage.selectFlight(0);
		bookingHomePage.clickFareSelectButton(0);
		Thread.sleep(3000);
		bookingHomePage.selectFlight(1);
		bookingHomePage.clickFareSelectButton(0);
		Thread.sleep(3000);
		bookingExtrasPage = bookingHomePage.clickContinueButton();
	}

	@Then("^Booking extras page is loaded$")
	public void booking_extras_page_is_loaded() throws Throwable {
		assertTrue(bookingExtrasPage.isInitialized());
	}

	@When("^I click check out and thanks button$")
	public void i_click_check_out_button() throws Throwable {
		bookingExtrasPage.clickCheckOutButton();
		bookingPaymentPage = bookingExtrasPage.clickOkThanksButton();
	}

	@Then("^Booking payment page is loaded$")
	public void booking_payment_page_is_loaded() throws Throwable {
		assertTrue(bookingPaymentPage.isInitialized());
	}

	@When("^I login with valid credentials$")
	public void i_login_with_valid_credentials() throws Throwable {
		bookingPaymentPage.clickLoginButton();
		bookingPaymentPage.enterCredentials("d.korczynski@o2.pl", "secretPassword1");
		bookingPaymentPage.selectKeepMeLoggedIn(false);
		bookingPaymentPage.clickLogInAuthorizationButton();
		Thread.sleep(1000);
	}

	@When("^I enter passenger and contact details$")
	public void i_enter_passenger_and_contact_details() throws Throwable {
		bookingPaymentPage.enterPassengerDetails("Mr", "John", "Smith");
		Thread.sleep(1000);
		bookingPaymentPage.enterContactDetails("790123456");
		Thread.sleep(1000);
	}

	@When("^I enter payment method with too short card number$")
	public void i_enter_payment_method_with_too_short_card_number() throws Throwable {
		bookingPaymentPage.enterPaymentMethod("0123456789", "123", "John Smith");
		Thread.sleep(1000);
	}

	@When("^I enter billing address$")
	public void i_enter_billing_address() throws Throwable {
		bookingPaymentPage.enterBillingAddress("Sun Lane", "Sun Land", "Dublin", "12345");
		Thread.sleep(1000);
	}

	@When("^I click pay now button$")
	public void i_click_pay_now_button() throws Throwable {
		bookingPaymentPage.clickPayNowButton(true);
		Thread.sleep(1000);
	}

	@Then("^I get error message that card number is too short$")
	public void i_get_error_message_that_card_number_is_too_short() throws Throwable {
		assertTrue(bookingPaymentPage.isErrorTooShortPresent());
		assertEquals("Card number is too short", bookingPaymentPage.getErrorTooShortMessage());
		Thread.sleep(5000);
	}

	@When("^I enter payment method with invalid card number$")
	public void i_enter_payment_method_with_invalid_card_number() throws Throwable {
		bookingPaymentPage.enterPaymentMethod("0123456789012345", "123", "John Smith");
		Thread.sleep(1000);
	}

	@Then("^I get error message that card number is invalid$")
	public void i_get_error_message_that_card_number_is_invalid() throws Throwable {
		assertTrue(bookingPaymentPage.isErrorInvalidPresent());
		assertEquals("Card number is invalid", bookingPaymentPage.getErrorInvalidMessage());
	}

}
