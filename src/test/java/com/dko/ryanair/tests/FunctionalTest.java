package com.dko.ryanair.tests;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FunctionalTest {

	protected static WebDriver driver;

	@BeforeClass
	public static void setUp() {
		System.out.println("setUp");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@After
	public void cleanUp() {
		System.out.println("cleanUp");
		driver.manage().deleteAllCookies();
	}

	@AfterClass
	public static void tearDown() {
		System.out.println("tearDown");
		driver.quit();
	}	
}
