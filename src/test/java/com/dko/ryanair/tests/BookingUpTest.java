package com.dko.ryanair.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.dko.ryanair.pageobjects.BookingExtrasPage;
import com.dko.ryanair.pageobjects.BookingHomePage;
import com.dko.ryanair.pageobjects.BookingPaymentPage;
import com.dko.ryanair.pageobjects.HomePage;

public class BookingUpTest extends FunctionalTest {

	@Test
	public void bookingUp() throws InterruptedException {
		driver.get("https://www.ryanair.com/ie/en/");

		HomePage homePage = new HomePage(driver);
		assertTrue(homePage.isInitialized());

		homePage.enterAirport("Bydgoszcz", "Dusseldorf Weeze");
		Thread.sleep(2000);
		homePage.clickContinueButton();
		homePage.enterFlyOutDate("05", "06", "2017");
		Thread.sleep(1000);
		homePage.enterFlyBackDate("09", "06", "2017");
		Thread.sleep(2000);

		BookingHomePage bookingHomePage = homePage.clickLetsGoButton();
		assertTrue(bookingHomePage.isInitialized());
		bookingHomePage.selectFlight(0);
		bookingHomePage.clickFareSelectButton(0);
		Thread.sleep(3000);
		bookingHomePage.selectFlight(1);
		bookingHomePage.clickFareSelectButton(0);
		Thread.sleep(3000);
		BookingExtrasPage bookingExtrasPage = bookingHomePage.clickContinueButton();
		assertTrue(bookingExtrasPage.isInitialized());

		bookingExtrasPage.clickCheckOutButton();
		BookingPaymentPage bookingPaymentPage = bookingExtrasPage.clickOkThanksButton();
		assertTrue(bookingPaymentPage.isInitialized());
		bookingPaymentPage.clickLoginButton();
		bookingPaymentPage.enterCredentials("d.korczynski@o2.pl", "secretPassword1");
		bookingPaymentPage.selectKeepMeLoggedIn(false);
		bookingPaymentPage.clickLogInAuthorizationButton();
		Thread.sleep(1000);
		bookingPaymentPage.enterPassengerDetails("Mr", "John", "Smith");
		Thread.sleep(1000);
		bookingPaymentPage.enterContactDetails("790123456");
		Thread.sleep(1000);
		bookingPaymentPage.enterPaymentMethod("0123456789", "123", "John Smith");
		Thread.sleep(1000);
		bookingPaymentPage.enterBillingAddress("Sun Lane", "Sun Land", "Dublin", "12345");
		Thread.sleep(1000);
		bookingPaymentPage.clickPayNowButton(true);
		assertTrue(bookingPaymentPage.isErrorTooShortPresent());
		assertEquals("Card number is too short", bookingPaymentPage.getErrorTooShortMessage());
		
		Thread.sleep(5000);
		bookingPaymentPage.enterPaymentMethod("0123456789012345", "123", "John Smith");
		Thread.sleep(1000);
		bookingPaymentPage.clickPayNowButton(true);
		Thread.sleep(1000);
		assertTrue(bookingPaymentPage.isErrorInvalidPresent());
		assertEquals("Card number is invalid", bookingPaymentPage.getErrorInvalidMessage());
	}	
	
}